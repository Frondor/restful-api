'use strict'

const { test, trait } = use('Test/Suite')('Create Gallery')

trait('DatabaseTransactions')
trait('Test/ApiClient')

test('Create and fetch a gallery', async ({ client }) => {
  const gallery = {
    title: 'Test gallery'
  }

  const location = await client.post('/galleries').send(gallery).end()
  location.assertStatus(201)
  location.assertJSONSubset({
    location: {}
  })

  const response = await client.get('/galleries').end()

  response.assertStatus(200)
  response.assertJSONSubset({
    data: [
      {
        "title": gallery.title,
        "pictures": []
      }
    ],
    _fromCache: undefined
  })
})

test('Fetch galleries through redis cache', async ({ client }) => {
  const response = await client.get('/galleries').end()
  response.assertStatus(200)
  response.assertJSONSubset({
    data: [{}],
    _fromCache: true
  })
})
